from flask import Flask, url_for, abort, request, jsonify
from flask_sqlalchemy import SQLAlchemy
import pgpy


app = Flask(__name__)
app.config.from_pyfile('config.py')
db = SQLAlchemy(app)


try:
    key, _ = pgpy.PGPKey.from_file('./server_key.asc')
except Exception:
    """Create a pgp key_pair"""
    from pgpy.constants import (PubKeyAlgorithm, KeyFlags, HashAlgorithm,
                                SymmetricKeyAlgorithm, CompressionAlgorithm)
    from datetime import timedelta
    key = pgpy.PGPKey.new(PubKeyAlgorithm.RSAEncryptOrSign, 4096)
    uid = pgpy.PGPUID.new('0ts')
    key.add_uid(
            uid,
            usage={KeyFlags.Sign},
            hashes=[HashAlgorithm.SHA512, HashAlgorithm.SHA256],
            ciphers=[SymmetricKeyAlgorithm.AES256],
            compression=[CompressionAlgorithm.BZ2, CompressionAlgorithm.Uncompressed],
            key_expires=timedelta(days=365)
    )
    with open("server_key.asc", "w") as k:
        k.write(str(key))


class Warehouse(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    fingerprint = db.Column(db.String(50))
    bin_file = db.Column(db.String(20))
    secret = db.Column(db.String(64))
    added_date = db.Column(db.Date)

    def __init__(self, fingerprint, bin_file, secret, added_date):
        self.fingerprint = fingerprint
        self.bin_file = bin_file
        self.secret = secret
        self.added_date = added_date


@app.route('/')
def api_root():
    message = """e0rs, an ephemeral, zero-trust, RESTful, fileshare service.
- Server's public key is located at /key
- Incoming packages should be POSTed to /share
- Packages awaiting recepit be found at /announced
- To receive a package GET from /receieve/{package_id}
- To acknowledge receipt and remove package, decrypt your
  received package, and then POST the SECRET contained
  with in /receive/{package_id}/ack

For more information, check us out on github:
     https://github.com/pard68/0ts
"""
    return message

@app.route('/key', methods = ['GET'])
def send_public_key():
    return jsonify({'public_key': str(key.pubkey)})

@app.route('/share/', methods = ['POST'])
def share():
    if not request.json:
        return request.json
    new_share = Warehouse(
            request.json.fingerprint,
            request.json.bin_file,
            request.json.secret,
            request.json.added_date
    )
    db.session.add(new_share)
    db.session.commit()
    return 201

@app.route('/announced')
def announcements():
    return jsonify([{'fingerprint': f, 'added': a}
                    for f,a in Warehouse.query.get(fingerprint, added_date)])

if __name__ == '__main__':
    app.run()

